package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.User;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author da
 * @Date 2021/8/15 下午 2:08
 */
public interface UserMapper extends BaseMapper<User>
{
}
