import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'Layout',
        component: () => import('@/layout/Layout'),
        // 设置重定向和子路由
        redirect: '/user',
        children: [
            {
                path: 'user',
                name: 'User',
                component: () => import('@/views/User'),
            },
            {
                path: 'book',
                name: 'Book',
                component: () => import('@/views/Book'),
            },
            {
                path: 'person',
                name: 'Person',
                component: () => import('@/views/Person'),
            }
        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login')
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('@/views/Register')
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
